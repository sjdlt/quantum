import os
import random
import mysql.connector
import string
from mysql.connector import connection, errorcode

db = os.environ.get('MYSQL_DATABASE', 'default_database')
add_session = """
    INSERT INTO {}.sessions(
        user_email,
        user_first_name,
        user_last_name,
        screen_width,
        screen_height,
        visits,
        page_response,
        domain,
        path
    ) VALUES (
        %(email)s,
        LOWER(%(first_name)s),
        LOWER(%(last_name)s),
        LOWER(%(screen_width)s),
        %(screen_height)s,
        %(visits)s,
        %(page_response)s,
        LOWER(%(domain)s),
        LOWER(%(path)s)
    )
""".format(db)

paths = [
  '/',
  '/benefits/',
  '/features/machine-intelligence/'
  '/features/session-replay',
  '/features/funnel-dna',
  '/features/behavior-analysis',
  '/features/dashboards',
  '/features/anomaly-detection',
  '/features/event-engine',
  '/platform/architecture-deployment',
  '/platform/security-gdpr',
  '/platform/native-mobile-apps',
  '/platform/certified-integrations',
  '/platform/unrivaled-performance',
  '/customers',
  '/company/'
]

screen_sizes = [
  (1024, 768),
  (1366, 768),
  (1280, 800),
  (1600, 900),
  (1536, 864)
]

first_names = [
  'Boba',
  'Han',
  'Luke',
  'Leia',
  'Lando',
  'Ben',
  'Anakin',
  'Wedge',
  'Ponda',
  'Darth',
  'Emperor'
]

last_names = [
  'Calrissian',
  'Skywalker',
  'Solo',
  'Kenobi',
  'Organa',
  'Dooku',
  'Palpatine',
  'Antilles',
  'Baba'
]

def random_string(N):
    return ''.join(random.choice(
        string.ascii_uppercase + 
        string.digits + 
        string.ascii_lowercase) for _ in range(N)
    )

def generate_session():
    screen_width, screen_height = random.choice(screen_sizes)
    visits = random.randrange(2,30)
    base_info = {
        'email': random_string(10) + '@gmail.com',
        'first_name': random.choice(first_names),
        'last_name': random.choice(last_names),
        'screen_width': screen_width,
        'screen_height': screen_height,
        'visits': visits,
        'domain': 'www.quantummetric.com'
    }
    return [
        {
            **base_info,
            **{
                'path': random.choice(paths),
                'page_response': random.randrange(500, 3000)
            }
        }
        for _ in range(visits)
    ]

cnx = None
cursor = None

try:
    cnx = connection.MySQLConnection(
      user='root', 
      password=os.environ.get('MYSQL_ROOT_PASSWORD', 'root'),
      host='127.0.0.1',
      database=db
    )

    cursor = cnx.cursor()

    print('starting data generation....')

    for i in range(1000):
        if (i > 0) and (i % 100 == 0):
            print('{} sessions created'.format(i))
        cursor.executemany(add_session, generate_session())
        cnx.commit()

    print('data generation finished')
except mysql.connector.Error as err:
    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        print("Something is wrong with your user name or password")
    elif err.errno == errorcode.ER_BAD_DB_ERROR:
        print("Database does not exist")
    else:
        print(err)
finally:
    if cursor:
        cursor.close()
    if cnx:
        cnx.close()
