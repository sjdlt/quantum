#!/bin/bash

while true; do
  mysqladmin -u root -p$MYSQL_ROOT_PASSWORD version < /dev/null
  if [ $? -eq 0 ]; then
    echo "running load scripts"
    mysql -u root -p$MYSQL_ROOT_PASSWORD $MYSQL_DATABASE < /scripts/setup.sql
    
    if [ $? -eq 0 ]; then
      echo "checking data"
      datacount=$(mysql -u root -p$MYSQL_ROOT_PASSWORD $MYSQL_DATABASE -se "SELECT count(*) FROM sessions;")
      
      if [ "${datacount}" = "0" ]; then
        echo "loading data"
        python3 /scripts/data-loader.py
        echo "data loaded"
      fi
      
      echo "done loading scripts"
      break
    fi

    echo "setup failed, trying again later..."
  fi
  echo "mysql: database is unavailable, retrying later"
  sleep 2
done &

exec /entrypoint.sh "$@"
