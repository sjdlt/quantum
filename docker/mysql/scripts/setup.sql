CREATE TABLE IF NOT EXISTS sessions (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  user_email      VARCHAR(50) NOT NULL,
  user_first_name VARCHAR(20) NOT NULL,
  user_last_name  VARCHAR(20) NOT NULL,
  screen_width    INT,
  screen_height   INT,
  visits          INT,
  page_response   INT,
  domain          VARCHAR(50),
  path            VARCHAR(100)
);

CREATE USER IF NOT EXISTS 'app_user'@'%' IDENTIFIED BY 'app_password';
GRANT SELECT, INSERT, UPDATE ON default_database.sessions TO 'app_user'@'%';