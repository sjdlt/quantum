#!/bin/sh

echo 'started'
set -eu
echo `env`
echo `ls -a /home/node/app`
cd $PROJECT_HOME
npm prune
npm cache clean --force --silent
npm install --silent --production
node ./bin/www
