'use strict';

module.exports = {
  mysql: {
    host: process.env.MYSQL_HOST || 'localhost',
    user: process.env.MYSQL_USER || 'app_user',
    password: process.env.MYSQL_PASSWORD || 'app_password',
    database: process.env.MYSQL_DATABASE || 'default_database'
  }
}
