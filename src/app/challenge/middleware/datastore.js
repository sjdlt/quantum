'use strict';

const util = require('util');
const mysql = require('mysql');
const config = require('../config/config');

const connection = mysql.createConnection(config.mysql);
const query = util.promisify(connection.query).bind(connection);

/**
 * Simple middleware that adds the connection to the locals area
 * for use by downstream middleware.
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
module.exports = {
  middleware: (req, res, next) => {
    res.locals.db = query;
    next();
  },
  connection
}