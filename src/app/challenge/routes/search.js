'use string';

const express = require('express');
const router = express.Router();

const validators = require('../utils/validators');
const queryBuilder = require('../utils/query-builder');

router.get('/', async (req, res, next) => {
  const data = { message: 'only POST is supported' }

  try {
    console.log('getting data');
    const rows = await res.locals.db('SELECT count(*) from sessions');
    console.log(`got data ${ rows }`);
    data.rows = rows;
  } catch (error) {
    console.log(error);
  }

  res.status(404).json(data);
});

router.post('/', async (req, res, next) => {
  const response = { success: false };

  if (validators.isValidSearchRequest(req.body)) {
    try {
      const queryInfo = queryBuilder.buildQuery(req.body.filters);
      const rows = await res.locals.db(queryInfo.sql, queryInfo.params);
      response.rows = rows;
      response.debugSql = queryInfo.debugSql;
      response.success = true;
      console.log(`generated sql: ${ response.debugSql }`);
    } catch (error) {
      console.log(error);
    }
  } else {
    response.error = 'invalid inputs detected';
  }

  res.json(response)
});

module.exports = router;