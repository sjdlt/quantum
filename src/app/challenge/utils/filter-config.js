/*
 * A set of operators that validate and format the inputs for the given 
 * operation.
 */ 
const validators = require('./validators');
const _ = require('lodash');

/**
 * 
 * @param {String} value The value to format.
 * @param {RegExp} regex Optional regex to apply to remove invalid characters.  
 */
const _stringFormatter = (value, regex) => {
  let formattedValue = (!_.isNil(regex)) ? value.replace(regex, '') : value;
  return _.trim(formattedValue).toLowerCase();
};

/**
 * 
 * @param {Number|String} value 
 */
const _numberFormatter = (value) => (_.isNumber(value)) 
  ? value 
  : _.toNumber( value.replace(/,/g, '') );

const FIELDS = {
  email: {
    operatorType: 'string',
    schema: {
      fieldName: 'user_email',
      minLength: 1,
      maxLength: 50,
      valueFormatter: (value) => _stringFormatter(value, /[^A-Za-z0-9@\.-_]/g)
    }
  },
  screen_width: {
    operatorType: 'numeric',
    schema: {
      fieldName: 'screen_width',
      min: 480,
      max: 7680,
      valueFormatter: _numberFormatter
    }
  },
  screen_height: {
    operatorType: 'numeric',
    schema: {
      fieldName: 'screen_height',
      min: 480,
      max: 7680,
      valueFormatter: _numberFormatter
    }
  },
  num_visits: {
    operatorType: 'numeric',
    schema: {
      fieldName: 'visits',
      min: 1,
      max: 100,
      valueFormatter: _numberFormatter
    }
  },
  first_name: {
    operatorType: 'string',
    schema: {
      fieldName: 'user_first_name',
      minLength: 1,
      maxLength: 20,
      valueFormatter: (value) => _stringFormatter(value, /[^A-Za-z\s-]/g)
    }
  },
  last_name: {
    operatorType: 'string',
    schema: {
      fieldName: 'user_last_name',
      minLength: 1,
      maxLength: 20,
      valueFormatter: (value) => _stringFormatter(value, /[^A-Za-z\s-]/g)
    }
  },
  page_response_time: {
    operatorType: 'numeric',
    schema: {
      fieldName: 'page_response',
      min: 1,
      max: 10000,
      valueFormatter: _numberFormatter
    }
  },
  domain: {
    operatorType: 'string',
    schema: {
      fieldName: 'domain',
      minLength: 1,
      maxLength: 50,
      valueFormatter: (value) => _stringFormatter(value, /[^A-Za-z0-9\.-]/g)
      
    }
  },
  path: {
    operatorType: 'string',
    schema: {
      fieldName: 'path',
      minLength: 1,
      maxLength: 100,
      valueFormatter: (value) => _stringFormatter(value, /[^A-Za-z0-9\.-_\?]/g)
    }
  }
};

const OPERATORS = {
  range: {
    numeric: {
      validator: validators.isValidRange,
      builder: (schema, values) => {
        const gte = queryBuilder.buildPredicate({ 
          schema, 
          operator: '>=', 
          values: values[0] 
        });
        const lte = queryBuilder.buildPredicate({ 
          schema, 
          operator: '<=', 
          values: values[1] 
        });
        
        return {
          query:  `(${gte.query} AND ${lte.query})`,
          params: [gte.params[0], lte.params[0]]
        }
      }
    }
  },
  lte: {
    numeric: {
      validator: validators.validateFirstNumericEntry,
      builder: (schema, values) => queryBuilder.buildPredicate({
        schema, 
        values: values[0], 
        operator: '<='
      })
    }
  },
  equals: {
    numeric: {
      validator: validators.validateFirstNumericEntry,
      builder: (schema, values) => queryBuilder.buildPredicate({
        schema, 
        values: values[0], 
        operator: '='
      })
    },
    string: {
      validator: validators.validateFirstStringEntry,
      builder: (schema, values) => queryBuilder.buildPredicate({
        schema, 
        values: values[0], 
        operator: '='
      })
    }
  },
  not_equals: {
    numeric: {
      validator: validators.validateFirstNumericEntry,
      builder: (schema, values) => queryBuilder.buildPredicate({
        schema, 
        values: values[0], 
        operator: '!='
      })
    },
    string: {
      validator: validators.validateFirstStringEntry,
      builder: (schema, values) => queryBuilder.buildPredicate({
        schema, 
        values: values[0], 
        operator: '!='
      })
    }
  },
  gte: {
    numeric: {
      validator: validators.validateFirstNumericEntry,
      builder: (schema, values) => queryBuilder.buildPredicate({
        schema, 
        values: values[0], 
        operator: '>='
      })
    }
  },
  starts_with: {
    string: {
      validator: validators.validateFirstStringEntry,
      builder: (schema, values) => queryBuilder.buildPredicate({
        schema,
        values,
        operator: 'LIKE',
        sqlFormatter: (value) => `${value}%`
      })
    }
  },
  not_starts_with: {
    string: {
      validator: validators.validateFirstStringEntry,
      builder: (schema, values) => queryBuilder.buildPredicate({
        schema,
        values,
        operator: 'NOT LIKE',
        sqlFormatter: (value) => `${value}%`
      })
    }
  },
  contains: {
    string: {
      validator: validators.validateFirstStringEntry,
      builder: (schema, values) => queryBuilder.buildPredicate({
        schema,
        values,
        operator: 'LIKE',
        sqlFormatter: (value) => `%${value}%`
      })
    }
  },
  not_contains: {
    string: {
      validator: validators.validateFirstStringEntry,
      builder: (schema, values) => queryBuilder.buildPredicate({
        schema,
        values,
        operator: 'NOT LIKE',
        sqlFormatter: (value) => `%${value}%`
      })
    }
  },
  in: {
    string: {
      validator: validators.validateStringArray,
      builder: (schema, values) => queryBuilder.buildPredicate({
        schema,
        values,
        operator: "="
      })
    }
  },
  not_in: {
    string: {
      validator: validators.validateStringArray,
      builder: (schema, values) => queryBuilder.buildPredicate({
        schema,
        values,
        operator: "!=",
        intersection: true
      })
    }
  },
  all: {
    string: {
      validator: validators.validateStringArray,
      builder: (schema, values) => queryBuilder.buildPredicate({
        schema, 
        values, 
        operator: 'LIKE', 
        intersection: true,
        sqlFormatter: (value) => `%${value}%`
      })
    }
  }
};

module.exports = {
  FIELDS,
  OPERATORS
};

// Fix circular dependency.
const queryBuilder = require('./query-builder');
