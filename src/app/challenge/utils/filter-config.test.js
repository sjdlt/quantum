'use strict';

const filterConfig = require('./filter-config');

describe('OPERATORS', () => {
  test('not_equals', () => {
    (filterConfig.OPERATORS.not_equals);
  });

  test('range', () => {
    const builder = filterConfig.OPERATORS.range.numeric.builder;

    const schema = filterConfig.FIELDS.num_visits.schema;
    const params = [20, 50]
    const results = builder(schema, params);

    expect( params ).toEqual(results.params);
    expect( results.query ).toEqual('(visits >= ? AND visits <= ?)');
  });
});
