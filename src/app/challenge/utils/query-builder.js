'use strict';

const mysql = require('mysql');
const _ = require('lodash');
const config = require('../config/config');

const queryPreamble = `SELECT * FROM ${ config.mysql.database }.sessions WHERE`;
const queryPostfix = ' LIMIT 20';

const _formatString = (value, subRegex, formatter) => {
  value = _.trim( value.replace(subRegex, '') ).toLowerCase();

  if (formatter) {
    value = formatter(value);
  }

  return value;
}

/**
 * A convenience method used to build the predicate portion of the query. The
 * following options 
 * 
 * @param {Array} values          The values used for the predicate comparison.
 * @param {Object} schema         The schema of the field being processed.
 * @param {String} operator       The symbol to use as the operator.
 * @param {Boolean} intersection  Set to 'true' to use an intersection of
 *                                the values (e.g. 'AND'); otherwise a union 
 *                                ('OR') will be used.
 * @param {Function} sqlFormatter Optional formatter that will be applied
 *                                after the schema's valueFormatter has
 *                                been applied.
 * 
 * @returns An object with the conversion results. The return object will
 *          contain two properties: 
 *          * query: the SQL snippet 
 *          * params: the associated parameters
 * 
 * @throws Error Thrown when the schema is invalid.
 */
const buildPredicate = (options) => {
  let { schema, values, operator, intersection, sqlFormatter } = options;

  if (_.isNil(schema) || _.isNil(schema.fieldName || _.isNil(schema.valueFormatter))) {
    throw new Error('A valid schema was not used');
  }

  sqlFormatter = sqlFormatter || ((value) => value);

  const field = schema.fieldName;
  operator = _.trim(operator);

  if (_.isString(values) || _.isNumber(values)) {
    values = sqlFormatter( schema.valueFormatter(values) );

    return {
      query: `${ field } ${ operator } ?`, 
      params: [ values ]
    }
  } else if (_.isArray(values) && !_.isEmpty(values)) {
    const separator = intersection ? ' AND ' : ' OR '
    const preparedValues = [];
    
    const predicates = values.map( (value) => {
      value = sqlFormatter( schema.valueFormatter(value) );
      preparedValues.push( value );
      return `${ field } ${ operator } ?`
    });
    
    const query = _.join(predicates, separator);
    
    return {
      query: (values.length > 1) ? `(${query})` : query,
      params: preparedValues
    }
  } else {
    throw new Error('An invalid predicate value was used. Use only numbers, strings, or arrays');
  }
};

/**
 * Builds a query from the request. This code assumes that the request has passed
 * through the validation suite beforehand - no guarantees are made if that isn't 
 * the case.
 * 
 * @param {*} filterRequest 
 */
const buildQuery = (filterRequest) => {
  if (_.isNil(filterRequest) || !_.isArray(filterRequest) || _.isEmpty(filterRequest)) {
    throw new Error('invalid filterRequest object used.');
  }

  const predicates = [];
  const params = [];

  filterRequest.forEach( (filter) => {
    const fieldInfo = filterConfig.FIELDS[filter.field.toLowerCase()];
    const operatorType = fieldInfo.operatorType;

    const operatorInfo = filterConfig.OPERATORS[filter.operator.toLowerCase()][operatorType];
    const preparedQuery = operatorInfo.builder(fieldInfo.schema, filter.values);

    predicates.push(preparedQuery.query);
    preparedQuery.params.forEach((item) => {
      params.push(item);
    });
  });

  const sql = `${ queryPreamble } ${ _.join(predicates, ' AND ') } ${queryPostfix};`
  return {
    debugSql: mysql.format(sql, params),
    sql,
    params
  }
}

module.exports = {
  buildPredicate,
  buildQuery,
  __TEST__: {
    queryPreamble,
    queryPostfix
  }
};

// Fix circular dependency.
const filterConfig = require('./filter-config');
