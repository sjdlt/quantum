'use strict';

const queryBuilder = require('./query-builder');
const filterConfig = require('./filter-config');
const _ = require('lodash');
const valueFormatter = (value) => _.trim(value);
const numberFormatter = (value) => (_.isNumber(value)) 
  ? value 
  : _.toNumber( value.replace(/,/g, '') );

describe('buildPredicate', () => {
  const buildPredicate = queryBuilder.buildPredicate;

  test('should produce the correct output for single values.', () => {
    let output = buildPredicate({ 
      schema: { fieldName: 'email', valueFormatter }, 
      values: ' hello ', 
      operator: '!='
    });
    let expected = {
      query: 'email != ?',
      params: ['hello']
    }
    expect(output).toEqual(expected);

    output = buildPredicate({ 
      schema: { fieldName: 'visits', valueFormatter: numberFormatter } ,
      values: '9',
      operator: '='
    });
    expected = {
      query: 'visits = ?',
      params: [9]
    }
    expect(output).toEqual(expected);
  });

  test('should throw an error when invalid values are used', () => {
    expect( () => buildPredicate({
      schema: { fieldName: 'email', valueFormatter }, 
      values: { hello: 'world' }, 
      operator: '!='
    }) ).toThrow();
  });

  test('should produce the correct output for multiple inputs values', () => {
    let params = ['hello', 'world'];
    let expected = {
      query: '(email != ? AND email != ?)',
      params
    }
    let output = buildPredicate({ 
      schema: { fieldName: 'email', valueFormatter }, 
      values: params, 
      operator: '!=', 
      intersection: true
    });
    expect(output).toEqual(expected);

    params = [1,2,3];
    output = buildPredicate({ 
      schema: { fieldName: 'visits', valueFormatter: numberFormatter },
      values: params, 
      operator: '='
    });
    expected = {
      query: '(visits = ? OR visits = ? OR visits = ?)',
      params
    }
    expect(output).toEqual(expected);
  });

  test('should properly apply the sql formatter', () => {
    let expected = {
      query: 'email LIKE ?',
      params: ['email%']
    }
    let output = buildPredicate({ 
      schema: { fieldName: 'email', valueFormatter }, 
      values: ['email'], 
      operator: 'LIKE',
      sqlFormatter: (value) => `${value}%`
    });
    expect(output).toEqual(expected);   
  });

});

describe('#buildQuery', () => {
  const buildQuery = queryBuilder.buildQuery;
  const preamble = queryBuilder.__TEST__.queryPreamble;
  const postfix = queryBuilder.__TEST__.queryPostfix;

  test('should throw an error when an invalid inputs are used', () => {
    expect( () => buildQuery() ).toThrow();
    expect( () => buildQuery('abc') ).toThrow();
    expect( () => buildQuery([]) ).toThrow();
  });

  test('should produce the correct sql for a single input', () => {
    const filter = {
      field: 'last_name',
      operator: 'not_equals',
      values: [' sKywalker ', 'solo']
    };

    const inputs = { 
      schema: { fieldName: 'user_last_name', valueFormatter }, 
      values: filter.values[0],
      operator: '!='
    };

    const processedPredicates = queryBuilder.buildPredicate(inputs);
    const results = buildQuery([filter]);

    expect(results.sql).toBe(`${preamble} ${processedPredicates.query} ${postfix};`);
    expect(results.params.length).toBe(1);
    expect(results.params[0]).toBe(_.trim(filter.values[0]).toLowerCase());
  });

  test('should produce the correct sql for multiple inputs', () => {
    const filters = [
      {
        field: 'last_name',
        operator: 'not_equals',
        values: [' skywalker ', 'solo']
      },
      {
        field: 'first_name',
        operator: 'in',
        values: ['han', 'luke']
      }
    ];

    const lastNameSql = 'user_last_name != ?';
    const numVisitsSql = '(user_first_name = ? OR user_first_name = ?)';

    const expectedSql = `${preamble} ${lastNameSql} AND ${numVisitsSql} ${postfix};`;
    const results = buildQuery(filters);

    expect(results.sql).toEqual(expectedSql);
    expect(results.params.length).toEqual(3);
    expect(results.params).toEqual(['skywalker', 'han', 'luke']);
    console.log(results.debugSql);
  });
});

