'use strict';

/*
 * This module contains various functions used for validating a 
 * filter request object.
 */

const _ = require('lodash');

/**
 * Validates that the input is a non-empty string.
 * 
 * @param {String} value  The value to validate
 * @param {Object} schema The schema to validate against
 * 
 * @returns true if the value is a non-empty string 
 * @throws Thrown when the schema or the schema.subRegex values
 *         are missing.
 */
const isValidString = (value, schema) => {
  if (_.isNil(schema) || _.isNil(schema.valueFormatter)) {
    throw new Error('A valid schema must be provided');
  }

  let valid = false;

  if (!_.isNil(value) && _.isString(value)) {
    const formattedValue = schema.valueFormatter(value);
    const minLength = (!_.isNil(schema.minLength))
      ? schema.minLength
      : 1;
    const maxLength = (!_.isNil(schema.maxLength))
      ? schema.maxLength
      : formattedValue.length;

    valid = (minLength <= formattedValue.length) && (formattedValue.length <= maxLength);
  }
  
  return valid;
}

/**
 * Helper function to determine if a value is a number. It will accept 
 * numbers and numeric strings.
 * 
 * @param {Number|String} value The input value to validate.
 * @param {Object} schema The schema to validate against
 * 
 * @returns true if the input is a number. 
 */
const isValidNumber = (value, schema) => {
  schema = schema || {};
  let valid = false;

  if (!_.isNil(value) && (_.isNumber(value) || _.isString(value))) {
    if (_.isString(value)) {
      value = value.trim();
      valid = value.length > 0;
    } else {
      valid = true;
    }

    if (valid) {
      const convertedValue = schema.valueFormatter(value);
      valid = !_.isNaN(convertedValue);

      if (valid && !_.isNil(schema.min)) {
        valid = convertedValue >= schema.min;
      }
    
      if (valid && !_.isNil(schema.max)) {
        valid = convertedValue <= schema.max;
      }
    }
  }

  return valid;
};

/**
 * Validates that the number range meets the following criteria:
 * 
 * 1. The first two entries in the array are numbers.
 * 2. The numbers meet the schema constraints, if any.
 * 2. The first number is less than the second number.
 * 3. The 
 * 
 * @param {Array} values  The array of values to validate.
 * @param {Object} schema The schema for the field.
 * 
 * @return true if valid entries were found
 */
const isValidRange = (values, schema) => {
  schema = schema || {};
  console.log(`value range (${values}) validation for ${schema.fieldName}`);

  let valid = _.isArray(values) && (values.length >= 2);

  if (valid) {
    const lower = (isValidNumber(values[0], schema))
      ? schema.valueFormatter(values[0])
      : NaN;
    const upper = (isValidNumber(values[1], schema))
      ? schema.valueFormatter(values[1])
      : NaN;

    valid = !_.isNaN(lower) && !_.isNaN(upper) && (lower < upper);
  }

  console.log(`valid: ${valid}`);

  return valid;
}

/**
 * Validates that the given array's values are valid, as determined
 * by the validator function. Note that there must be at least 
 * one entry in the list, and every entry in the list must be valid. 
 * 
 * Both an empty list or a list with any non-valid values will
 * return a value of 'false'.
 * 
 * @param {Array} values       The list of values to validate.  
 * @param {Array} slice        The subset of the list to consder (optional).
 * @param {function} validator The validator function. Must accept a
 *                             single value and return a boolean result.
 * 
 * @returns true if the list contains all non empty/null strings.
 * @throws Error when the slice is invalid.
 */
const validateArray = (values, schema, validator, slice) => {
  values = values || [];
  slice = slice || [];

  if (!_.isArray || _.isEmpty(values)) {
    return false;
  }

  if (!_.isEmpty(slice)
      && ((slice.length != 2) || (slice[0] >= slice[1]) || (slice[1] > values.length))) {
    throw new Error(`An invalid slice value was used ${ slice }`);
  }
  
  if (!_.isEmpty(slice)) {
    values = values.slice(slice[0], slice[1]);
  }

  return values.every((value) => validator(value, schema));
};

/** 
 * Convenience functions for the operators.
 */
const validateStringArray = (values, schema, slice) => validateArray(values, schema, isValidString, slice);
const validateFirstStringEntry = (values, schema) => validateStringArray(values, schema, [0, 1]);
const validateNumericArray = (values, schema, slice) => validateArray(values, schema, isValidNumber, slice);
const validateFirstNumericEntry = (values, schema) => validateNumericArray(values, schema, [0, 1]);

/**
 * Validates the filter using the following logic:
 * 
 * 1. filter.field must be present and must be configured in the filter-config
 *    FIELDS configuration.
 * 2. filter.operator must be:
 *    a. A string value that is configured in the filter-config OPERATORS configuration
 *    b. A valid operator for the field type as determined by the FIELDS config.
 * 3. filter.values must be:
 *    a. A non-empty array of values.
 *    b. A valid set of values, as determined by the OPERATOR config.
 * 
 * @param {object} filter  The filter to evaluate.
 * 
 * @returns true if the filter is found to be valid. 
 */
function isValidFilter(filter) {
  filter = filter || {};

  const field = _.trim(filter.field).toLowerCase();
  const operatator = _.trim(filter.operator).toLowerCase();
  const values = (_.isArray(filter.values)) ? filter.values : [];

  const fieldConfig = filterConfig.FIELDS[field];
  const operatorConfig = filterConfig.OPERATORS[operatator];

  const validStructure = !_.isNil(fieldConfig)
    && !_.isNil(operatorConfig)
    && fieldConfig.operatorType in operatorConfig;

  const validatedFilter = (validStructure)
    ? operatorConfig[fieldConfig.operatorType].validator(values, fieldConfig.schema)
    : false;

  console.log(`${field} has a valid structure: ${validStructure}`);
  console.log(`${field} has a valid filter: ${validatedFilter}`);

  return validatedFilter;
};

/**
 * Validates a search request. To be valie, the request must conform to the structure 
 * shown below and each filter in the 'filters' array must pass its respective 
 * validation criteria.
 * 
 * Struture example:
 * {
 *  filters: [
 *    { field: '...', operator: '...', values: [...] },
 *    ...
 *  ]
 * }
 * 
 * @param {Object} filterSet 
 * 
 * @returns true if the request is valid.
 */
function isValidSearchRequest(searchRequest) {
  return typeof searchRequest === 'object'
         && searchRequest !== null
         && !_.isNil(searchRequest.filters)
         && _.isArray(searchRequest.filters)
         && searchRequest.filters.every(isValidFilter);
}

module.exports = {
  isValidRange,
  isValidString,
  isValidNumber,
  validateArray,
  validateStringArray,
  validateFirstStringEntry,
  validateNumericArray,
  validateFirstNumericEntry,
  isValidFilter,
  isValidSearchRequest
}

// Circular reference fix. Probably should be refactored.
const filterConfig = require('./filter-config');