'use strict';

const validators = require('./validators');
const _ = require('lodash');

const numberFormatter = (value) => (_.isNumber(value)) 
  ? value 
  : _.toNumber( value.replace(/,/g, '') );

const defaultStringSchema = {
  valueFormatter: (value) => _.trim(value.replace(/[^A-Za-z]/g, '')).toLowerCase()
};

describe('#isValidString', () => {
  const isValidString = validators.isValidString;

  test('should throw an error if the schema is missing', () => {
    expect( () => isValidString() ).toThrow();
    expect( () => isValidString('hello', {}) ).toThrow();
    expect( () => isValidString('hello', { valueFormatter: null }) ).toThrow();
  });

  test('should recognize invalid strings', () => {
    expect( isValidString(undefined, defaultStringSchema) ).toBe(false);
    expect( isValidString([], defaultStringSchema) ).toBe(false);
    expect( isValidString('  ', defaultStringSchema) ).toBe(false);
    expect( isValidString(null, defaultStringSchema) ).toBe(false);

    const tempSchema = { 
      valueFormatter: (value) => value,
      minLength: 5
    };
    
    expect( isValidString('abcd', tempSchema)).toBe(false);

    tempSchema.maxLength = 9;
    expect( isValidString('skywalkers', tempSchema)).toBe(false);
  });

  test('should recognize valid strings', () => {
    expect( isValidString(' asd ', defaultStringSchema) ).toBe(true);
  });

});

describe('validateStringArray', () => {
  const validateStringArray = validators.validateStringArray;

  test('should throw an error when an invalid slice is provided', () => {
    expect( () => validateStringArray(['luke'], defaultStringSchema, [0]) ).toThrow();
    expect( () => validateStringArray(['luke'], defaultStringSchema, [1, 0]) ).toThrow();
    expect( () => validateStringArray(['luke'], defaultStringSchema, [1, 1]) ).toThrow();
    expect( () => validateStringArray(['luke'], defaultStringSchema, [0, 2]) ).toThrow();
  });

  test('should recognize invalid inputs', () => {
    expect( validateStringArray(['han', null, 'greedo'], defaultStringSchema) ).toBe(false);
    expect( validateStringArray(['han', null, 'greedo'], defaultStringSchema, [1,2]) ).toBe(false);
    expect( validateStringArray([null, null, null], defaultStringSchema) ).toBe(false);
    expect( validateStringArray(['han', []], defaultStringSchema) ).toBe(false);
  });

  test('should recognize valid inputs', () => {
    expect( validateStringArray(['chewie', 'lando', 'r2'], defaultStringSchema) ).toBe(true);
    expect( validateStringArray(['chewie'], defaultStringSchema) ).toBe(true);
    expect( validateStringArray(['chewie', 'lando', 'r2'], defaultStringSchema, [1, 3]) ).toBe(true);
  });
});

describe('validateFirstStringEntry', () => {
  const validateFirstStringEntry = validators.validateFirstStringEntry;
  
  test('should correctly detect invalid values', () => {
    expect( validateFirstStringEntry([' ', 'shot', 'first'], defaultStringSchema) ).toBe(false);
    expect( validateFirstStringEntry([null, 'shot', 'first'], defaultStringSchema) ).toBe(false);
  });

  test('should accept valid values', () => {
    expect( validateFirstStringEntry(['han', 'shot', 'first'], defaultStringSchema) ).toBe(true);
    expect( validateFirstStringEntry(['han', null, null], defaultStringSchema) ).toBe(true);
  });
});

describe('isValidNumber', () => {
  const isValidNumber = validators.isValidNumber;
  const defaultSchema = { valueFormatter: numberFormatter };

  test('should reject invalid values', () => {
    expect( isValidNumber(undefined, defaultSchema) ).toBe(false);
    expect( isValidNumber('a', defaultSchema) ).toBe(false);
    expect( isValidNumber('  ', defaultSchema) ).toBe(false);
    expect( isValidNumber([], defaultSchema) ).toBe(false);
  });

  test('should validate valid values', () => {
    expect( isValidNumber(' 1 ', defaultSchema) ).toBe(true);
    expect( isValidNumber('1.1', defaultSchema) ).toBe(true);
    expect( isValidNumber(2, defaultSchema) ).toBe(true);
    expect( isValidNumber(0, defaultSchema) ).toBe(true);
    expect( isValidNumber(-1.1, defaultSchema) ).toBe(true);
  });

  test('should properly handle min value settings', () =>{
    const schema = _.cloneDeep(defaultSchema);
    schema.min = 5;
    
    expect( isValidNumber('9', schema)).toBe(true);
    expect( isValidNumber(4, schema)).toBe(false);
  });

  test('should properly handle max settings', () => {
    const schema = _.cloneDeep(defaultSchema);
    schema.max = 10

    expect( isValidNumber(10, schema) ).toBe(true);
    expect( isValidNumber(11, schema) ).toBe(false);
  });

  test('should properly hanlde min and max settings', () => {
    const schema = _.cloneDeep(defaultSchema);
    schema.min = 10;
    schema.max = 100;
    
    expect( isValidNumber(20, schema) ).toBe(true);
    expect( isValidNumber(9, schema) ).toBe(false);
    expect( isValidNumber('101', schema) ).toBe(false);
  });
});

describe('isValidRange', () => {
  const isValidRange = validators.isValidRange;
  const defaultSchema = { valueFormatter: numberFormatter };

  test('should detect invalid inputs', () => {
    expect( isValidRange() ).toBe(false);
    expect( isValidRange('hello', defaultSchema) ).toBe(false);
    expect( isValidRange([1], defaultSchema) ).toBe(false);
    expect( isValidRange(['h', 1], defaultSchema) ).toBe(false);
    expect( isValidRange([30, 1], defaultSchema) ).toBe(false);
  });

  test('should detect valid and invalid ranges', () => {
    const schema = _.cloneDeep(defaultSchema);
    schema.min = 20;
    schema.max = 50;

    expect( isValidRange([1,40], schema) ).toBe(false);
    expect( isValidRange([20,51], schema) ).toBe(false);
    expect( isValidRange([1,51], schema) ).toBe(false);
    expect( isValidRange([20,50], schema) ).toBe(true);
  })
});

describe('isValidFilter', () => {
  const isValidFilter = validators.isValidFilter;
  let baseFilter;

  beforeEach(() => {
    baseFilter = {
      field: 'last_name',
      operator: 'equals',
      values: ['skywalker'] 
    }
  });

  test('should detect an invalid field', () => {
    delete baseFilter.field;
    expect(isValidFilter(baseFilter)).toBe(false);

    baseFilter.field = [];
    expect(isValidFilter(baseFilter)).toBe(false);

    baseFilter.field = '  ';
    expect(isValidFilter(baseFilter)).toBe(false);
  });
  
  test('should detect an invalid operator', () => {
    delete baseFilter.operator;
    expect(isValidFilter(baseFilter)).toBe(false);

    baseFilter.operator = 123;
    expect(isValidFilter(baseFilter)).toBe(false);

    baseFilter.operator = 'gte';
    expect(isValidFilter(baseFilter)).toBe(false);
  });
  
  test('should detect an invalid value array', () => {
    delete baseFilter.values;
    expect(isValidFilter(baseFilter)).toBe(false);

    baseFilter.values = 'abcc';
    expect(isValidFilter(baseFilter)).toBe(false);

    baseFilter.values = [];
    expect(isValidFilter(baseFilter)).toBe(false);

    baseFilter.values = [1];
    expect(isValidFilter(baseFilter)).toBe(false);

    baseFilter.values = ['  '];
    expect(isValidFilter(baseFilter)).toBe(false);
  });
  
  test('should allow a valid filter to pass', () => {
    baseFilter.field = ' LAST_NAME ';
    expect(isValidFilter(baseFilter)).toBe(true);
  });
});

describe('isValidSearchRequest', () => {
  const isValidSearchRequest = validators.isValidSearchRequest;
  let baseFilter;

  beforeEach(() => {
    baseFilter = {
      field: 'last_name',
      operator: 'equals',
      values: ['skywalker'] 
    }
  });

  test('should detect an invalid object', () => {
    expect(isValidSearchRequest()).toBe(false);
    expect(isValidSearchRequest([])).toBe(false);
    expect(isValidSearchRequest({})).toBe(false);
    expect(isValidSearchRequest({filters: null})).toBe(false);
    expect(isValidSearchRequest({filters: 'abc'})).toBe(false);
  });
  
  test('should allow a valid object to pass through', () => {
    const secondFilter = _.cloneDeep(baseFilter);
    secondFilter.field = 'last_name';
    const filters = { filters: [baseFilter, secondFilter] }

    expect(isValidSearchRequest(filters)).toBe(true);
  });
});